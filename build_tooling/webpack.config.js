const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = ({
  debug = false,
} = {}) => {
  const plugins = [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(debug
        ? 'development'
        : 'production'),
    }),
    new HtmlWebpackPlugin({
      inject: true,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyURLs: true,
      },
    }),
  ];
  if (!debug) {
    plugins.push(new webpack.optimize.UglifyJsPlugin({
      sourceMap: 'source-map',
      compress: {
        screw_ie8: true,
        warnings: false,
      },
      mangle: {
        screw_ie8: true,
      },
      output: {
        comments: false,
      },
    }));
  }

  return {
    target: 'web',
    devtool: 'source-map',
    entry: './src/application.js',
    output: {
      path: path.resolve(__dirname, 'www'),
      filename: debug
        ? 'bundle.js'
        : 'bundle.min.js',
      publicPath: '',
    },
    plugins,
    module: {
      rules: [
        {
          test: /\.js$/,
          include: [path.resolve(__dirname, 'src')],
          loader: 'babel-loader',
          query: {
            compact: true,
            presets: [
              [
                'es2015', {
                  modules: false,
                },
              ],
            ],
          },
        },
      ],
    },
    performance: {
      hints: false,
    },
  };
};

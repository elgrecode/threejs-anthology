### ThreeJS Anthology
This is a repository that is dedicated towards building UI with ThreeJS. It will create small projects as studies to figure out the best way to have ThreeJS components create delightful user experiences.

#### Motivations
Today, users expect animations that rival native devices. The web can offer that ability through WebGL, but currently the tech is new and not being utilized to enhance standard web browsing experiences. The projects here will focus on creating standard patterns that every day web users are comfortable and competent using. The aim here is to create enhanced components that will utilize the GPU and not suck.

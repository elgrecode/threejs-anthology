/* eslint-env browser */
/* eslint-disable no-use-before-define */
import Detector from 'imports-loader?THREE=three!../lib/Detector';
import * as Three from 'three';
import Stats from '../lib/Stats';
import dat from '../lib/dat.gui.min.js';
const gui = new dat.GUI();

const {
  Scene,
  Fog,
  WebGLRenderer,
  LinearFilter,
  PerspectiveCamera,
  Mesh,
  MeshBasicMaterial,
  MeshStandardMaterial,
  NearestFilter,
  Plane,
  PlaneBufferGeometry,
  Texture,
  TextureLoader,
  UVMapping,
  Vector3,
} = Three;


if (!Detector.webgl) Detector.addGetWebGLMessage();

const SCREEN_WIDTH = window.innerWidth;
const SCREEN_HEIGHT = window.innerHeight;

let container,
  stats;

let camera,
  scene,
  scene2,
  materialPainting,
  mode = {},
  renderer;
window.THREE = Three;


let mouseX = 0,
  mouseY = 0;

const windowHalfX = window.innerWidth / 2;
const windowHalfY = window.innerHeight / 2;

init();
animate();


function init() {
  container = document.createElement('div');
  document.body.appendChild(container);

  camera = new PerspectiveCamera(35, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 5000);
  camera.position.z = 1500;

  window.scene = scene = new Scene();
  scene2 = new Scene();

  const specialPlane = new Plane(new Vector3(0, 1, 0).applyAxisAngle(
    new Vector3(0, 0, 1), 0.895398
  ), 0);


  // PAINTING
  const callbackPainting = () => {
    const image = texturePainting.image;

    texturePainting2.image = image;
    texturePainting2.needsUpdate = true;

    const geometry = new PlaneBufferGeometry(25, 25);
    const mesh = new Mesh(geometry, materialPainting);
    const mesh2 = new Mesh(geometry, materialPainting2);

    addPainting(scene, mesh);
    addPainting(scene2, mesh2);

    function addPainting(zscene, zmesh) {
      zmesh.scale.x = image.width / 100;
      zmesh.scale.y = image.height / 100;

      zscene.add(zmesh);
      zscene.add(specialPlane);
    }
  };


  const clipPlanes = [
    specialPlane
  ];

  var texturePainting = new TextureLoader().load('../static/fruit.jpg', callbackPainting);
  var texturePainting2 = new Texture();
  materialPainting = new MeshBasicMaterial({
    color: 0xffffff,
    clippingPlanes: clipPlanes,
    clipIntersection: true,
    map: texturePainting
  });
  var materialPainting2 = new MeshBasicMaterial({ color: 0xffccaa, map: texturePainting2 });

  texturePainting2.minFilter = texturePainting2.magFilter = NearestFilter;
  texturePainting.minFilter = texturePainting.magFilter = LinearFilter;
  texturePainting.mapping = UVMapping;


  window.renderer = renderer = new WebGLRenderer({ antialias: true });
  renderer.setClearColor(0xffffff);
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
  renderer.autoClear = false;
  renderer.localClippingEnabled = true;

  renderer.domElement.style.position = 'relative';
  container.appendChild(renderer.domElement);

  stats = new Stats();
  container.appendChild(stats.dom);

  document.addEventListener('mousemove', onDocumentMouseMove, false);
}


// GUI
mode.clipRotation = 0;
gui.add(mode, 'clipRotation', 0, 1).step(0.001);

function onDocumentMouseMove(event) {
  mouseX = (event.clientX - windowHalfX);
  mouseY = (event.clientY - windowHalfY);
}


function animate() {
  requestAnimationFrame(animate);
  // const clippingPane = materialPainting.clippingPlanes[0];
  // clippingPane.normal.applyAxisAngle(
  //   new Vector3(0, 0, 1), mode.clipRotation
  // );

  render();
  stats.update();
}

function render() {
  camera.position.x += (mouseX - camera.position.x) * 0.05;
  camera.position.y += (-(mouseY - 200) - camera.position.y) * 0.05;

  camera.lookAt(scene.position);
  renderer.clear();
  renderer.setScissorTest(true);
  renderer.setScissor(0, 0, (SCREEN_WIDTH / 2) - 2, SCREEN_HEIGHT);
  renderer.render(scene, camera);
  renderer.setScissor(SCREEN_WIDTH / 2, 0, (SCREEN_WIDTH / 2) - 2, SCREEN_HEIGHT);
  renderer.render(scene2, camera);
  renderer.setScissorTest(false);
}

/* eslint-env browser */
/* eslint-disable no-use-before-define */
import Detector from 'imports-loader?THREE=three!../lib/Detector';
import TrackballControls from 'imports-loader?THREE=three!../lib/TrackballControls';
import * as Three from 'three';
import Stats from '../lib/Stats';

const {
  Scene,
  FogExp2,
  WebGLRenderer,
  PerspectiveCamera,
  CylinderGeometry,
  MeshPhongMaterial,
  FlatShading,
  Mesh,
  DirectionalLight,
  AmbientLight,
} = Three;

if (!Detector.webgl) Detector.addGetWebGLMessage();

let container;
let stats;
let camera;
let controls;
let scene;
let renderer;
let light;


function init() {
  camera = new PerspectiveCamera(60, window.innerWidth / window.innerHeight, 1, 1000);
  camera.position.z = 500;

  // Trackball Controls
  controls = new TrackballControls(camera);
  controls.rotateSpeed = 1.0;
  controls.zoomSpeed = 1.2;
  controls.panSpeed = 0.8;
  controls.noZoom = false;
  controls.noPan = false;
  controls.staticMoving = true;
  controls.dynamicDampingFactor = 0.3;

  controls.keys = [65, 83, 68];

  controls.addEventListener('change', render);

  // world
  scene = new Scene();
  scene.fog = new FogExp2(0xcccccc, 0.002);

  const geometry = new CylinderGeometry(0, 10, 30, 4, 1);
  const material = new MeshPhongMaterial({ color: 0xffffff, shading: FlatShading });

  for (let i = 0; i < 500; i += 1) {
    const mesh = new Mesh(geometry, material);
    mesh.position.x = (Math.random() - 0.5) * 1000;
    mesh.position.y = (Math.random() - 0.5) * 1000;
    mesh.position.z = (Math.random() - 0.5) * 1000;
    mesh.updateMatrix();
    mesh.matrixAutoUpdate = false;
    scene.add(mesh);
  }


  // lights
  light = new DirectionalLight(0xffffff);
  light.position.set(1, 1, 1);
  scene.add(light);

  light = new DirectionalLight(0x002288);
  light.position.set(-1, -1, -1);
  scene.add(light);

  light = new AmbientLight(0x222222);
  scene.add(light);


  // renderer
  renderer = new WebGLRenderer({ antialias: false });
  renderer.setClearColor(scene.fog.color);
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);

  container = document.getElementById('root');
  container.appendChild(renderer.domElement);

  stats = new Stats();
  document.getElementById('stats').appendChild(stats.dom);
  window.addEventListener('resize', onWindowResize, false);
  render();
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
  controls.handleResize();
  render();
}

function animate() {
  requestAnimationFrame(animate);
  controls.update();
}

function render() {
  renderer.render(scene, camera);
  stats.update();
}

init();
animate();

/* eslint-env browser */
/* eslint-disable no-use-before-define */
import Detector from 'imports-loader?THREE=three!../lib/Detector';
import * as Three from 'three';
import OC from 'three-orbit-controls';
import Stats from '../lib/Stats';
import dat from '../lib/dat.gui.min.js';

const OrbitControls = OC(Three);


const {
  AmbientLight,
  Color,
  Fog,
  WebGLRenderer,
  CanvasTexture,
  DoubleSide,
  LinearFilter,
  HemisphereLight,
  RepeatWrapping,
  Mesh,
  MeshBasicMaterial,
  MeshStandardMaterial,
  NearestFilter,
  Object3D,
  Plane,
  PlaneBufferGeometry,
  PerspectiveCamera,
  Scene,
  SphereBufferGeometry,
  Texture,
  TextureLoader,
  UVMapping,
  Vector3,
} = Three;

let camera,
  mode,
  scene,
  stats,
  startTime,
  time,
  renderer;
let group;
init();
animate();
function init() {
  camera = new PerspectiveCamera(40, window.innerWidth / window.innerHeight, 1, 800);
  camera.position.set(-20, 10, 50);
  camera.lookAt(new Vector3(0, 0, 0));
  window.scene = scene = new Scene();


  // Lights
  const light = new HemisphereLight(0xffffbb, 0x080820, 1);
  scene.add(light);

  const specialPlane = new Plane(new Vector3(1, 0, 0).applyAxisAngle(
    new Vector3(0, 0, 1), 0.495398
  ), 0);
  specialPlane.material = new MeshBasicMaterial({ color: 0x2b4c3f });


  const clipPlanes = [
    specialPlane,
    new Plane(new Vector3(0, -1, 0), 0),
    new Plane(new Vector3(0, 0, -1), 0)
  ];
  scene.add(new AmbientLight(0x505050));
  group = new Object3D();
  for (let i = 1; i < 25; i++) {
    const geometry = new SphereBufferGeometry(i / 2, 48, 48);
    const material = new MeshStandardMaterial({
      color: new Color(Math.sin(i * 0.5) * 0.5 + 0.5, Math.cos(i * 1.5) * 0.5 + 0.5, Math.sin(i * 4.5 + 0) * 0.5 + 0.5),
      roughness: 0.95,
      metalness: 0.0,
      side: DoubleSide,
      clippingPlanes: clipPlanes,
      clipIntersection: true
    });
    group.add(new Mesh(geometry, material));
  }
  scene.add(group);
				// Renderer
  const container = document.body;
  window.renderer = renderer = new WebGLRenderer();
  renderer.antialias = true;
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.setClearColor(0x222222);
  renderer.localClippingEnabled = true;
  window.addEventListener('resize', onWindowResize, false);
  container.appendChild(renderer.domElement);
				// Stats
  stats = new Stats();
  container.appendChild(stats.dom);
				// GUI
  mode = {};
  mode.clipIntersection = true;
  mode.clipPosition = 0;
  const gui = new dat.GUI();
  gui.add(mode, 'clipIntersection').onChange(() => {
    const children = group.children;
    for (let i = 0; i < children.length; i++) {
      const child = children[i];
      child.material.clipIntersection = !child.material.clipIntersection;
    }
  });
  gui.add(mode, 'clipPosition', -16, 16);
				// Controls
  const controls = new OrbitControls(camera, renderer.domElement);
  controls.target.set(0, 1, 0);
  controls.update();
				// Start
  startTime = Date.now();
  time = 0;
}
function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
}
function animate() {
  requestAnimationFrame(animate);
  const children = group.children;
  for (let i = 0; i < children.length; i++) {
    const current = children[i].material;
    for (let j = 0; j < current.clippingPlanes.length; j++) {
      const plane = current.clippingPlanes[j];
      plane.constant = (49 * plane.constant + mode.clipPosition) / 50;
    }
  }
  stats.begin();
  renderer.render(scene, camera);
  stats.end();
}

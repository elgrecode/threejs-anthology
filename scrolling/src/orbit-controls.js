/* eslint-env browser */
/* eslint-disable no-use-before-define */
import Detector from 'imports-loader?THREE=three!../lib/Detector';
import * as Three from 'three';
import OC from 'three-orbit-controls';
import Stats from '../lib/Stats';

const OrbitControls = OC(Three);

const {
  Scene,
  FogExp2,
  WebGLRenderer,
  PerspectiveCamera,
  CylinderGeometry,
  MeshPhongMaterial,
  FlatShading,
  Mesh,
  DirectionalLight,
  AmbientLight,
} = Three;

if (!Detector.webgl) Detector.addGetWebGLMessage();

let light;
let camera;
let controls;
let scene;
let statsContainer;
let renderer;


function init() {
  scene = new Scene();
  scene.fog = new FogExp2(0xcccccc, 0.002);

  renderer = new WebGLRenderer();
  renderer.setClearColor(scene.fog.color);
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);

  const container = document.getElementById('root');
  container.appendChild(renderer.domElement);

  camera = new PerspectiveCamera(60, window.innerWidth / window.innerHeight, 1, 1000);
  camera.position.z = 500;

  controls = new OrbitControls(camera, renderer.domElement);
  controls.addEventListener('change', render); // remove when using animation loop
    // enable animation loop when using damping or autorotation
    // controls.enableDamping = true;
    // controls.dampingFactor = 0.25;
  controls.enableZoom = false;
  controls.autoRotate = true;

  // world
  const geometry = new CylinderGeometry(0, 10, 30, 4, 1);
  const material = new MeshPhongMaterial({
    color: 0xffffff,
    shading: FlatShading,
  });

  for (let i = 0; i < 500; i += 1) {
    const mesh = new Mesh(geometry, material);
    mesh.position.x = (Math.random() - 0.5) * 1000;
    mesh.position.y = (Math.random() - 0.5) * 1000;
    mesh.position.z = (Math.random() - 0.5) * 1000;
    mesh.updateMatrix();
    mesh.matrixAutoUpdate = false;
    scene.add(mesh);
  }

  // lights
  light = new DirectionalLight(0xffffff);
  light.position.set(1, 1, 1);
  scene.add(light);

  light = new DirectionalLight(0x002288);
  light.position.set(-1, -1, -1);
  scene.add(light);

  light = new AmbientLight(0x222222);
  scene.add(light);

  // stats
  statsContainer = new Stats();
  document.getElementById('stats').appendChild(statsContainer.domElement);

  window.addEventListener('resize', onWindowResize, false);
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate() {
  statsContainer.begin();
  requestAnimationFrame(animate);
  controls.update(); // required if controls.enableDamping = true, or if controls.autoRotate = true
  statsContainer.end();
  render();
}

function render() {
  renderer.render(scene, camera);
}

init();
render(); // remove when using next line for animation loop (requestAnimationFrame)
animate();

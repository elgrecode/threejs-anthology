module.exports.HTMLWebpackPluginDefaults = {
  title: 'ThreeJS Controls',
  inject: true,
  template: 'src/application.ejs',
  minify: {
    removeComments: true,
    collapseWhitespace: true,
    removeRedundantAttributes: true,
    useShortDoctype: true,
    removeEmptyAttributes: true,
    removeStyleLinkTypeAttributes: true,
    keepClosingSlash: true,
    minifyJS: true,
    minifyURLs: true,
  },
};

module.exports.entry = {
  OrbitControls: './src/orbit-controls.js',
  TrackballControls: './src/trackball-controls.js',
  Painting: './src/painting.js',
  ClippedPainting: './src/clipped-painting.js',
  Clipping: './src/clipping.js',
};

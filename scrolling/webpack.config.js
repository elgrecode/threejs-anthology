/* eslint-disable */
const path = require('path');
const webpack = require('webpack');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const { entry, HTMLWebpackPluginDefaults } = require('./webpack/settings');

module.exports = ({ debug = false } = {}) => {
  const plugins = [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(debug
        ? 'development'
        : 'production'),
    }),
  ];
  // Builds different HTML outputs
  Object.keys(entry).forEach((e) => {
    plugins.push(
      new HTMLWebpackPlugin(Object.assign({chunks: [e], filename: `${e}.html`}, HTMLWebpackPluginDefaults))
    )
  });

  if (!debug) {
    plugins.push(new webpack.optimize.UglifyJsPlugin({
      sourceMap: 'source-map',
      compress: {
        screw_ie8: true,
        warnings: false,
      },
      mangle: {
        screw_ie8: true,
      },
      output: {
        comments: false,
      },
    }));
  }

  return {
    target: 'web',
    devtool: 'source-map',
    entry,
    output: {
      path: path.resolve(__dirname, 'www'),
      filename: debug
        ? '[name].js'
        : '[name].min.js',
      publicPath: '',
    },
    plugins,
    module: {
      rules: [
        {
          test: /\.js$/,
          include: [path.resolve(__dirname, 'src')],
          loader: 'babel-loader',
          query: {
            compact: true,
            presets: [
              [
                'es2015', {
                  modules: false,
                },
              ],
            ],
          },
        },
      ],
    },
    performance: {
      hints: false,
    },
  };
};
